# Simulation

The web application **Simulation** includes a _Scenario Planer_ to support planning different scenarios and includes a _Component Catalog_ to list various simulation tools and models to add assistance to plan co-simulation of various scenarios by addressing typical use cases in interdisciplinary research. The platform also provides the possibility to run example simulations directly via the platform. The focus lies on the combination of different domain-specific simulation tools and modeling approaches.

## Getting started
For the locally deployment on Linux, you can install Docker Compose. [Here](https://docs.docker.com/compose/install/linux/#install-the-plugin-manually) you can find an introduction on how to install Docker Compose under Linux.

For all Windows users check out [Docker Desktop](https://www.docker.com/products/docker-desktop/).


## Deploy locally using Docker Compose

First you need to **build** the docker image for the simulation web application:
```
docker build -t simulation_webapp_image .
```

Next, you **run** the docker image with _docker compose_. This will also run two other containers, one for the postgres DB (_simulation_postgres_db_) and one for the nginx server (_nginx_).
```
docker compose -f docker-compose.yml up -d
```

Next, you need to connect to the _simulation_webapp_ container that is already running and get a bash shell in the container with the following command:

```
docker exec -it simulation_webapp /bin/bash
```
Inside the bash shell, you need to run the following command to **collect** all necessary **static files** for this project (e.g., images, css, javascript):
```
python manage.py collectstatics
```

Finally, you can open a browser and navigate to **http://localhost/simulation/**. You should see the starting page of the Simulation web application.
