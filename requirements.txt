django                  == 4.0.6
requests                == 2.28.2
django-crispy-forms     == 1.14.0
django-filter           == 22.1
django-multiselectfield == 0.1.12
djangorestframework     == 3.13.1
django-extensions       == 3.2.1
psycopg2==2.8.6
