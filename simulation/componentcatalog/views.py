from django.db.models import Q
from django.shortcuts import render, redirect
from django.urls import reverse
from django.http import HttpResponse, HttpResponseNotFound
from django.core.serializers import serialize

from functools import reduce
from operator import or_

from .forms import ComponentForm, MoreSettingsForm, MoreSelectionsForm
from .models import Component


def component_catalog(response):
    # Use response.session to save selected component information/input (should map to the component model)
    if response.method == 'GET':

        component_form = ComponentForm(
            initial=response.session.get('scenario', {}).get('Component'))
        more_settings_form = MoreSettingsForm(
            initial=response.session.get('scenario', {}).get('Component'))
        more_selections_form = MoreSelectionsForm(
            initial=response.session.get('scenario', {}).get('Component'))

        return render(response, 'componentcatalog/component_catalog.html',
                    {'component_form': component_form,
                    'more_settings_form': more_settings_form,
                    'more_selections_form': more_selections_form,
                    'component_list': Component.objects.none()})

    if response.method == 'POST':
        component_form = ComponentForm(response.POST)

        if 'scenario' not in response.session:
            # Create empty scenario in session
            print('# Create empty scenario in session')
            response.session['scenario'] = {}

        # TODO Use CoSiCoCa and add infos of component to session
        data_new = []
        # Get previous saved session components
        previous_components = response.session.get(
            'scenario', {}).get('Components')
        print(f'Previous Component: {previous_components}')

        if previous_components:
            for c in previous_components:
                data_new.append(c)

        # Get selected elements in Table 'Matching Components'
        selected_components = Component.objects.filter(
            id__in=response.POST.getlist('tids'))
        print(f'Selected Components: {selected_components}')

        # If the component is stored in the DB,
        # it is enough to pass the id to the session
        if selected_components:
            for c in selected_components:
                data_new.append(str(c.id))

        response.session['scenario']['Components'] = data_new
        response.session.modified = True

        # redirect to the scenario planner page (reverse() is needed to preserve language prefix in url)
        redirect_url = f'{reverse("plan-scenario")}'
        return redirect(redirect_url)


def matching_components(request):

    if request.method == 'GET':
        sectors = request.GET.getlist('sector', None)
        functions_in_sector = request.GET.getlist('function_in_sector', None)

        sector_query = None
        function_in_sector_query = None

        # Use reduce() to iterate over all selected options ('Heat, Electricity') of a specitic filter (e.g., 'languages')
        # to combine a series of Q(..) objects into a larger query combined with '| OR' operations.
        # ('**' expands dictionary key/value pairs to keyword argument - value pairs)
        if sectors:
            sector_query = reduce(or_, (Q(**{f'sector__icontains': f}) for f in sectors))

        if functions_in_sector:
            function_in_sector_query = reduce(or_, (Q(**{f'function_in_sector__icontains': f}) for f in functions_in_sector))

        # Check if Q objects (selected checkboxes) are not empty
        if sector_query and function_in_sector_query:
            matching_object_list = Component.objects.filter(sector_query & function_in_sector_query)
        elif sector_query:
            matching_object_list = Component.objects.filter(sector_query)
        elif function_in_sector_query:
            matching_object_list = Component.objects.filter(function_in_sector_query)
        else:
            matching_object_list = Component.objects.all()

        serialized_data = serialize('json', matching_object_list)
        print(f'Matching components in JSON: {serialized_data}')

        return HttpResponse(serialized_data, content_type='application/json')

    return HttpResponseNotFound("<h1>Page Not Found</h1>")
