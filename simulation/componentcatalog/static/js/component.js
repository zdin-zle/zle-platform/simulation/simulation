$(document).ready(function () {

    // SumoSelect enhances an HTML Select Box 
    // into a Single/Multiple option dropdown list.
    $("#id_type_of_software").SumoSelect({
        csvDispCount: 3
    });

    $("#id_access_via_api").SumoSelect({
        csvDispCount: 4
    });

    $("#id_energy_source").SumoSelect({
        csvDispCount: 4
    });

    $("#id_ict_type").SumoSelect({
        csvDispCount: 4
    });

    $("#id_demand_type").SumoSelect({
        csvDispCount: 4
    });

    $("#id_transfer_type").SumoSelect({
        csvDispCount: 4
    });

    $("#id_storage_type").SumoSelect({
        csvDispCount: 4
    });

    // check all checkboxes once after document is ready
    checkCheckboxes()
    // update table of matching components once after document is ready
    updateMatchingComponents()

    // check all checkboxes of function_in_sector if a change-event occurred
    $("#id_function_in_sector").on("change", function () { checkCheckboxes() })

    // Check all checkboxes of both forms if a change-event occurred
    $(".components-form-div input").on("change", function () {
        // Update table of matching components if checkbox was selected or unselected
        updateMatchingComponents()
    })

});


function updateMatchingComponents() {

    var checked_components_form = $('.components-form-div input:checked')
    var checked_selections_form = $('.selections-form input:checked')

    // Merge selected checkboxes from both forms
    var all_selected_checkboxes = $.merge($.merge([], checked_components_form), checked_selections_form);
    console.log(all_selected_checkboxes);

    filters = ''
    all_selected_checkboxes.forEach(element => {
        filters += '&' + element.name + '=' + element.value
    });

    let url = 'component-catalog/matching-components?' + filters;
    console.log(url);

    fetch(url)
        .then(response => response.json())
        .then(data => createTable(data));
}


function createTable(data) {
    // Loop through all matching components
    var tr = "";
    for (let i = 0; i < data.length; i++) {
        tr += "<tr>";
        tr += `<td class="pt-3-half"><input class="custom-checkbox" type="checkbox" name="tids" id="checkbox-${i}" value="${data[i].pk}"/></td>`;
        tr += `<td class="pt-3-half">${data[i].fields.name}</td>`;
        tr += `<td class="pt-3-half"><a href="${data[i].fields.link}">${data[i].fields.link}</td>`;
        tr += `<td class="pt-3-half">${data[i].fields.type_of_software}</td>`;
        tr += `<td class="pt-3-half">${data[i].fields.access_via_api}</td>`;
        tr += "</tr>"
    }

    // Append rows to <tbody class='matching_components'>
    $('.matching_components')[0].innerHTML = tr
}

function checkCheckboxes() {
    var something_selected = false;
    var selections = $("#id_function_in_sector").find('label');

    for (let index = 0; index < selections.length; index++) {
        const selection_name = selections[index].firstChild.value;
        //console.log(selection_name)

        if ($("#id_function_in_sector").find('input[value=' + selection_name + ']').is(':checked')) {
            something_selected = true;

            // if specific option is selected show a detailed selection for it
            if (selection_name == "GenerationConversion") {
                $('#div_id_energy_source').show();
            } else {
                $('#div_id_' + selection_name.toLowerCase() + '_type').show();
            }

        } else {

            // if specific option is unselected hide the detailed selection for it
            if (selection_name == "GenerationConversion") {
                $('#div_id_energy_source').hide();
            } else {
                $('#div_id_' + selection_name.toLowerCase() + '_type').hide();
            }
        }
    }

    if (something_selected) {
        $('#selections-form').slideDown(300);
    } else {
        $("#selections-form").slideUp(300);
    }
}
