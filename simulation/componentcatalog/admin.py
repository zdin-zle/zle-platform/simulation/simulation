from django.contrib import admin


from .models import Component

# Register your models here.
class ComponentAdmin(admin.ModelAdmin):
    pass

admin.site.register(Component, ComponentAdmin)