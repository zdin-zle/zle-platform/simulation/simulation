from django import forms
from django.forms import ModelForm

from .models import Component


class ComponentForm(ModelForm):
    # TODO Access ontology and Create Form from ontology
    # TODO List all options in ontology
    # TODO Iterate over all options and create form Field (Check if multipleChoice or DropDown)

    sector = forms.MultipleChoiceField(
        choices=Component.Sectors.choices, widget=forms.CheckboxSelectMultiple)
    function_in_sector = forms.MultipleChoiceField(
        choices=Component.Functions.choices, widget=forms.CheckboxSelectMultiple)

    class Meta:
        model = Component
        fields = ['sector', 'function_in_sector']
        # help_texts = {}


class MoreSelectionsForm(ModelForm):
    energy_source = forms.MultipleChoiceField(
        choices=Component.EnergySources.choices, widget=forms.SelectMultiple, label='Type of Energy Source', required=False)
    ict_type = forms.MultipleChoiceField(
        choices=Component.ICTType.choices, widget=forms.SelectMultiple, label='Type of ICT', required=False)
    storage_type = forms.MultipleChoiceField(
        choices=Component.StorageType.choices, widget=forms.SelectMultiple, label='Type of Storage', required=False)
    demand_type = forms.MultipleChoiceField(
        choices=Component.DemandType.choices, widget=forms.SelectMultiple, label='Type of Demand', required=False)
    transfer_type = forms.MultipleChoiceField(
        choices=Component.TransferType.choices, widget=forms.SelectMultiple, label='Type of Transfer', required=False)

    class Meta:
        model = Component
        fields = ['demand_type', 'energy_source', 'ict_type',
                  'storage_type', 'transfer_type']
        # help_texts = {}


class MoreSettingsForm(ModelForm):
    type_of_software = forms.MultipleChoiceField(
        choices=Component.TypeOfSoftware.choices, widget=forms.SelectMultiple, label='Type of Software', required=False)
    access_via_api = forms.MultipleChoiceField(
        choices=Component.AccessViaAPI.choices, widget=forms.SelectMultiple, label='Access via API', required=False)

    class Meta:
        model = Component
        fields = ['type_of_software', 'access_via_api']
        # help_texts = {}
