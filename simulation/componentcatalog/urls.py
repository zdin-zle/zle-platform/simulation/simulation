from django.urls import path
from . import views


urlpatterns = [
    path('component-catalog', views.component_catalog, name='component-catalog'),
    path('component-catalog/matching-components', views.matching_components, name='matching-components'),
    
    # TODO: Implement function to add new components to the DB
    #path('component-catalog/add', views.add, name='add-component'),
    #path('component-catalog/delete/<id>', views.delete, name='delete-component'),
]
