from django.db import models
from multiselectfield import MultiSelectField
# from django_extensions.db.fields import AutoSlugField


class Component(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    link = models.CharField(max_length=255)
    description = models.TextField(max_length=400)
    project_reference = models.CharField(
        max_length=255, null=True, blank=True, verbose_name='Project')
    contact = models.CharField(max_length=255, null=True, blank=True)

    # Type of Software
    class TypeOfSoftware(models.TextChoices):
        SIMULATION_MODEL = 'SimulationModel', 'Simulation Model'
        FRAMEWORK = 'ModellingFramework', 'Modelling Framework'
        DATA_ANALYSIS_TOOL = 'DataAnalysisTool', 'Data Analysis Tool'
        DATA_STORAGE_TOOL = 'DataStorageTool', 'Data Storage Tool'
        MODEL_COMPOSITOR = 'ModelCompositor', 'Model Compositor'
        CONTROLLER = 'Controller', 'Controller'
        ADAPTER_BINDING = 'AdapterBinding', 'Adapter/Binding'
        OTHER = 'Other', 'Other'

    type_of_software = MultiSelectField(
        choices=TypeOfSoftware.choices, null=True, blank=True, verbose_name='Type of Software')

    # Access via API options
    class AccessViaAPI(models.TextChoices):
        PYTHON = 'Python', 'Python'
        JAVA = 'Java', 'Java'
        C_Sharp = 'C#', 'C#'
        MATLAB = 'Matlab', 'Matlab'
        MODBUS = 'Modbus', 'Modbus'
        ZEROMQ = 'ZeroMQ', 'ZeroMQ'
        FMI = 'FMI', 'FMI'
        LOWLEVEL = 'lowlevel', 'Low-level'
        NONE = 'None', 'None'

    access_via_api = models.CharField(
        max_length=25, choices=AccessViaAPI.choices, null=True, blank=True, verbose_name='Access via API')

    # ------------------------------------
    # Informations for the ontology search
    # ------------------------------------
    # Sectors
    class Sectors(models.TextChoices):
        ELECTRICITY = 'Electricity', 'Electricity'
        HEAT = 'Heat', 'Heat'
        GAS = 'Gas', 'Gas'
        MOBILITY = 'Mobility', 'Mobility'

    sector = MultiSelectField(choices=Sectors.choices, null=True, blank=True, verbose_name='Sectors')

    # Functions
    class Functions(models.TextChoices):
        DEMAND = 'Demand', 'Demand'
        GENERATION_CONVERSION = 'GenerationConversion', 'Generation/Conversion'
        ICT = 'ICT', 'ICT'
        STORAGE = 'Storage', 'Storage'
        TRANSFER = 'Transfer', 'Transfer'

    function_in_sector = MultiSelectField(
        choices=Functions.choices, null=True, blank=True, verbose_name='Functions')

    # Type of Component for the ontology search
    # Energy Source
    class EnergySources(models.TextChoices):
        SOLAR_RADIATION = 'SolarRadiation', 'Solar Radiation'
        WIND = 'Wind', 'Wind'
        WATER = 'Water', 'Water'
        BIOGAS = 'Biogas', 'Biogas'
        HYDROGEN = 'Hydrogen', 'Hydrogen'
        BIOMASS = 'Biomass', 'Biomass'
        FUEL_GAS = 'FuelGas', 'Fuel/Gas'

    energy_source = MultiSelectField(
        choices=EnergySources.choices, null=True, blank=True, verbose_name='Type of Energy Source')

    # ICT Type
    class ICTType(models.TextChoices):
        METERING_SYSTEM = 'MeteringSystem', 'Metering System'
        COMMUNICATION = 'Communication', 'Communication'
        MANAGEMENT = 'Management', 'Management'
        MEASUREMENT = 'Measurement', 'Measurement'

    ict_type = MultiSelectField(
        choices=ICTType.choices, null=True, blank=True, verbose_name='Type of ICT')

    # Storage Type
    class StorageType(models.TextChoices):
        ELECTRICAL_STORAGE = 'ElectricalStorage', 'Electrical Storage'
        HYDROGEN_STORAGE = 'HydrogenStorage', 'Hydrogen Storage'
        WATER_TANK = 'WaterTank', 'Water Tank'
        VEHICLE = 'Vehicle', 'Vehicle'
        Buidling = 'Building', 'Building'

    storage_type = MultiSelectField(
        choices=StorageType.choices, null=True, blank=True, verbose_name='Type of Storage')

    # Demand Type
    class DemandType(models.TextChoices):
        BUSINESS = 'Business', 'Business'
        RESIDENT = 'Resident', 'Resident'
        INDUSTRY = 'Industry', 'Industry'
        VEHICLE = 'Vehicle', 'Vehicle'

    demand_type = MultiSelectField(
        choices=DemandType.choices, null=True, blank=True, verbose_name='Type of Demand')

    # Transfer Type
    class TransferType(models.TextChoices):
        POWER_GRID = 'PowerGrid', 'Power Grid'
        HEATING_NETWORK = 'HeatingNetwork', 'Heating Network'
        GAS_NETWORK = 'GasNetwork', 'Gas Network'

    transfer_type = MultiSelectField(
        choices=TransferType.choices, null=True, blank=True, verbose_name='Type of Transfer')
    # ------------------------------------

    class Meta:
        verbose_name_plural = 'Components'

    def __str__(self):
        return self.name
