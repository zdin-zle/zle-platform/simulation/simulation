from django.db import models
from multiselectfield import MultiSelectField
from django_extensions.db.fields import AutoSlugField

from componentcatalog.models import Component

FILTERS = ['language', 'domain']


class Scenario(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    description = models.TextField(max_length=400)
    link = models.CharField(max_length=255)
    tags = models.CharField(max_length=255)

    # Components
    components = models.ManyToManyField(
        Component, help_text='Add components to your scenario')

    # Filter options
    class ProgrammingLanguages(models.TextChoices):
        JAVA = 'Java', 'Java'
        PYTHON = 'Python', 'Python'
        CPLUSPLUS = 'CPlusPlus', 'C++'

    # Filter options
    class Domains(models.TextChoices):
        ELECTRICITY = 'Electricity', 'Electricity'
        HEATING = 'Heating', 'Heating'
        MOBILITY = 'Mobility', 'Mobility'
        WEATHER = 'Weather', 'Weather'
        COMMUNICATION = 'Communication', 'Communication'
        ECOLOGICAL = 'Ecological', 'Ecological'
        ECONOMICAL = 'Economical', 'Economical'
        SOCIAL = 'Social', 'Social'

    language = MultiSelectField(
        choices=ProgrammingLanguages.choices, null=True, blank=True, verbose_name='language')

    domain = MultiSelectField(
        choices=Domains.choices, null=True, blank=True, verbose_name='domain')

    slug = AutoSlugField(populate_from=['name'])

    class Meta:
        verbose_name_plural = 'Scenarios'

    def __str__(self):
        return self.name

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.prefetch_related('component')

    def get_components(self):
        return "\n".join([p.components for p in self.components.all()])
