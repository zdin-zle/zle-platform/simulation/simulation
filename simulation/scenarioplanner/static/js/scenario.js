$(document).ready(function () {
    const $tableID = $('#table');

    const newTr = `
         <tr class="hide">
           <td class="pt-3-half" contenteditable="true">Example</td>
           <td class="pt-3-half" contenteditable="true">Example</td>
           <td class="pt-3-half" contenteditable="true">Example</td>
           <td class="pt-3-half" contenteditable="true">Example</td>
           <td>
             <span class="table-remove"><button type="button" class="btn btn-danger btn-rounded btn-sm my-0 waves-effect waves-light">Remove</button></span>
           </td>
         </tr>`;

    $('.table-add').on('click', 'i', () => {
        const $clone = $tableID.find('tbody tr').last().clone(true).removeClass('hide table-line');

        if ($tableID.find('tbody tr').length === 0) {

            $('tbody').append(newTr);
        }

        $tableID.find('table').append($clone);
    });

    $tableID.on('click', '.table-remove', function () {
        console.log("Table Removed");
        $(this).parents('tr').detach();
    });

    $("#test-add-component").on("click", "a", () => {
        console.log("Button was clicked");
        $("#test-add-component").find("a").trigger("add-entry-to-table");
    });
});

$("#test-add-component").on('add-entry-to-table', function() {
    const $tableID = $('#table');

    const newTr = `
    <tr class="hide">
      <td class="pt-3-half" contenteditable="true">Example</td>
      <td class="pt-3-half" contenteditable="true">Example</td>
      <td class="pt-3-half" contenteditable="true">Example</td>
      <td class="pt-3-half" contenteditable="true">Example</td>
      <td>
        <span class="table-remove"><button type="button" class="btn btn-danger btn-rounded btn-sm my-0 waves-effect waves-light">Remove</button></span>
      </td>
    </tr>`;

    const $clone = $tableID.find('tbody tr').last().clone(true).removeClass('hide table-line');

    if ($tableID.find('tbody tr').length === 0) {

        $('tbody').append(newTr);
    }

    $tableID.find('table').append($clone);
});