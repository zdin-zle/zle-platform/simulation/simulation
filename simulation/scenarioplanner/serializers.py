from rest_framework import serializers
from .models import Scenario


ELEMENT_NAME = 'Simulation'
TYPE = 'Scenario'


class ScenarioSerializer(serializers.ModelSerializer):
    element_name = serializers.SerializerMethodField()  # add extra field
    type = serializers.SerializerMethodField()  # add extra field

    class Meta:
        model = Scenario
        fields = ['id', 'name', 'description',
                  'link', 'tags', 'element_name', 'type']

    def get_element_name(self, obj):
        return ELEMENT_NAME

    def get_type(self, obj):
        return TYPE
