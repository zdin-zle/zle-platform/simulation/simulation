from django.urls import path
from . import views


urlpatterns = [
    # simulation/scenario/
    path('scenario/', views.plan_scenario, name='plan-scenario'),
    path('scenario/component/<str:id>/remove', views.remove_component, name='remove-component'),
    path('scenario/component/remove-all', views.remove_all_components, name='remove-all-components'),

    # TODO: Create Example Scenarios view page
    path('scenario/example-scenarios', views.example_scenarios, name='example-scenarios'),
    # TODO: Get example scenario by slug id
    path('scenario/<slug:slug>', views.plan_scenario, name='plan-scenario'),

    # TODO: not implemented yet
    path('scenario/export', views.export_scenario, name='export-scenario'),
    path('scenario/save', views.save_scenario, name='save-scenario'),
    path('scenario/open', views.open_scenario, name='open-scenario'),
]
