from django.shortcuts import render, redirect
from django.urls import reverse

from componentcatalog.models import Component
from scenarioplanner.models import Scenario


def plan_scenario(response):
    # Use response.session to save scenario information/input (should map to the scenario model)
    # if no example scenario was selected: show empty form/data-table
    # else: use information of saved scenario in DB to pre-fill the session dictionary
    if 'scenario' in response.session:
        if 'Components' in response.session['scenario']:
            component_ids = response.session['scenario']['Components']
            print(f'Scenario Planner: {component_ids}')

            # Get selected elements in Table 'Matching Components'
            components = Component.objects.filter(id__in=component_ids)
            print(f'ScenarioPlanner --> Selected Components: {components}')

            return render(response, 'scenarioplanner/plan_scenario.html', {'scenario_components': components})

    return render(response, 'scenarioplanner/plan_scenario.html')


def remove_all_components(response):
    # delete previous scenario in session
    if 'scenario' in response.session:
        response.session['scenario'] = {}
        response.session.modified = True

    # redirect to the scenario planner page (reverse() is needed to preserve language prefix in url)
    redirect_url = f'{reverse("plan-scenario")}'
    return redirect(redirect_url)


def remove_component(response, id):
    # Remove component from current scenario (saved in response.session)
    if 'scenario' in response.session:
        component_ids = response.session['scenario']['Components']
        for component_id in component_ids:
            if component_id == id:
                component_ids.remove(id)

        response.session['scenario']['Components'] = component_ids
        response.session.modified = True

    # redirect to the scenario planner page (reverse() is needed to preserve language prefix in url)
    redirect_url = f'{reverse("plan-scenario")}'
    return redirect(redirect_url)


def example_scenarios(response):
    # List of existing scenarios (saved in the DB)
    # Redirect to 'simulation/scenario/<slug>' with a pre-filled scenario form (data table)
    # Include these information in response.session['<info>']

    scenarios = Scenario.objects.all()
    print(f'Examples Scenarios: {scenarios}')

    return render(response, 'scenarioplanner/example_scenarios.html', {'scenario_list': scenarios})


def export_scenario(response):
    # TODO: This will create an ontology-based scenario-file or
    # META-description that can be used within the scenario-file (demo.py) of mosaik
    pass


def save_scenario(reponse):
    # TODO: Save scenario locally (session file format?)
    pass


def open_scenario(response):
    # TODO: Open scenario from file system (session file format?)
    pass
