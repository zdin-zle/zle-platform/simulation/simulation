from django.urls import path
from . import views


urlpatterns = [
    path('', views.simulation_home, name='simulation-home'),

    # Needed for search API
    path("search/", views.scenarios, name="scenarios"),
    path("search/filters", views.filters, name="filters"),
]
