from django.shortcuts import render
from django.db.models import Q, Min, Max
from django.http import JsonResponse, HttpResponse
from django.core import serializers

from functools import reduce
from operator import or_
import json

from scenarioplanner.models import Scenario, FILTERS
from scenarioplanner.serializers import ScenarioSerializer


def simulation_home(response):
    return render(response, 'simulation/simulation_home.html')


def scenarios(request):
    query = request.GET.get('q')
    print(query)

    if query:
        object_list = Scenario.objects.filter(
            Q(name__icontains=query) | Q(description__icontains=query) |
            Q(tags__icontains=query) | Q(link__icontains=query)
        )
    else:
        object_list = Scenario.objects.all()

    for filter_name in FILTERS:
        filter_options = request.GET.getlist(filter_name)
        if filter_options:
            # Use reduce() to iterate over all selected options ('java, python') of a specitic filter (e.g., 'languages')
            # to combine a series of Q(..) objects into a larger query combined with '| OR' operations.
            # ('**' expands dictionary key/value pairs to keyword argument - value pairs)
            filter_query = reduce(
                or_, (Q(**{f'{filter_name}__icontains': f}) for f in filter_options))

            object_list = object_list.filter(filter_query)

    serializer = ScenarioSerializer(object_list, many=True)

    print(serializer.data)

    return JsonResponse(serializer.data, safe=False)


def filters(request):
    filters_response = []

    for filter in FILTERS:
        type = Scenario._meta.get_field(filter).get_internal_type()
        print(type)

        if type == 'CharField':
            options = []
            for choice_tupel in Scenario._meta.get_field(filter).get_choices(include_blank=False):
                options.append(choice_tupel[1])

            filter_dict = {
                'type': 'string_list',
                'field_name': Scenario._meta.get_field(filter).verbose_name,
                'options': options
            }

        # TODO: IntegerField is not supported yet
        # elif type == 'IntegerField':
        #     filter_dict = {
        #         'type': 'integer_range',
        #         'field_name': Scenario._meta.get_field(filter).verbose_name,
        #         'from': Scenario.objects.all().aggregate(min=Min(filter))['min'],
        #         'to':  Scenario.objects.all().aggregate(max=Max(filter))['max']
        #     }

        filters_response.append(filter_dict)

    print(filters_response)

    return JsonResponse(filters_response, safe=False)
